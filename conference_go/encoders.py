from common.json import ModelEncoder
from events.models import Conference, Location
from attendees.models import Attendee
from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet
from presentations.models import Presentation


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]

    def get_extra_data(self, o):
        return {"conference": o.conference.name, "status": o.status.name}


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
    ]

    def get_extra_data(self, o):
        return {"conference": o.conference.name}


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            dict = {}
            if hasattr(o, "get_api_url"):
                dict["href"] = o.get_api_url()
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                dict[property] = value
            dict.update(self.get_extra_data(o))
            return dict
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}


#   if the object to decode is the same class as what's in the
#   model property, then
#     * create an empty dictionary that will hold the property names
#       as keys and the property values as values
#     * for each name in the properties list
#         * get the value of that property from the model instance
#           given just the property name
#         * put it into the dictionary with that property name as
#           the key
#     * return the dictionary
#   otherwise,
#       return super().default(o)  # From the documentation
